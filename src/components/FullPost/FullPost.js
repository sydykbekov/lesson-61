import React, {Component} from 'react';
import './FullPost.css';
import axios from 'axios';

class FullPost extends Component {
    state = {
        loadedPost: null
    };

    componentDidUpdate() {
        const BASE_URL = 'https://jsonplaceholder.typicode.com';
        if (this.props.id) {
            if (!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== this.props.id)) {
                axios.get(BASE_URL + '/posts/' + this.props.id)
                    .then(response => {
                        this.setState({loadedPost: response.data});
                    });
            }
        }
    }


    render() {
        if (this.state.loadedPost) {
            return (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button className="Delete">Delete</button>
                    </div>
                </div>
            );
        }

        return <p style={{textAlign: 'center'}}>Please select a Post!</p>;
    }

}

export default FullPost;
