import React, {Component, Fragment} from 'react';
import './Blog.css';
import Post from "../../components/Post/Post";
import axios from 'axios';
import FullPost from "../../components/FullPost/FullPost";

class Blog extends Component {
    state = {
        posts: [],
        postsFormShown: false,
        selectedPostId: null
    };

    togglePostsForm = () => {
        this.setState(prevState => {
            return {postsFormShown: !prevState.postsFormShown};
        });
    };

    componentDidMount() {
        axios.get('/posts?_limit=4').then(response => {
            return Promise.all(response.data.map(post => {
                return axios.get('/posts?_limit=4/users/').then(response => {
                    return {...post, author: response.data.name};
                });
            }));
        }).then(results => {
            this.setState({posts: results});
        }).catch(error => {
            console.log(error);
        });
    }

    postSelectedHandler = id => {
        this.setState({selectedPostId: id});
    };

    render() {
        let postsForm = null;

        if (this.state.postsFormShown) {
            postsForm = (
                <section className="NewPost">
                    <p>New post form will be here</p>
                </section>
            );
        }

        return (
            <Fragment>
                <section className="Posts">
                    {this.state.posts.map(post => (
                        <Post key={post.id} title={post.title} author={post.author} clicked={() => this.postSelectedHandler(post.id)}/>
                    ))}
                </section>
                <section>
                    <FullPost id={this.state.selectedPostId} />
                </section>
                <button className="ToggleButton" onClick={this.togglePostsForm}>
                    New Post
                </button>
                {postsForm}
            </Fragment>
        );
    }
}

export default Blog;